package pl.lodz.p.iad.som.neurons;

import java.util.ArrayList;
import java.util.List;

import pl.lodz.p.iad.som.shapes.Point;
import pl.lodz.p.iad.som.shapes.Shape;

public class NeuronsGenerator {

	public static List<SimpleNeuralCell> getNeurons(int neuronsNumber, Shape shape) {
		List<SimpleNeuralCell> neurons = new ArrayList<>();
		for (int i = 0; i < neuronsNumber; i++) {
			Point point = shape.generatePoint();
			SimpleNeuralCell neuron = new SimpleNeuralCell(point.getX(), point.getY());
			neurons.add(neuron);
		}
		
		return neurons;
	}
		
}
