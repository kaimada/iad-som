package pl.lodz.p.iad.som.algorithm_k_means;

import java.awt.Color;
import java.awt.geom.Point2D;
import static java.awt.geom.Point2D.*;

import java.util.ArrayList;
import java.util.List;

import pl.lodz.p.iad.som.shapes.Point;

public class MosaicPart {
	private Point center;
	private List<Point> points;
	
	
	public MosaicPart(Point center){
		this.center = center;
		points = new ArrayList<>();
	}
	
	public void calculateCenter(){
		if(points.size() == 0)
			return;
		
        double xSum = 0;
        double ySum = 0;

        for (Point p : points) {
            xSum += p.getX();
            ySum += p.getY();
        }
        center = new Point(xSum / points.size(), ySum / points.size());
	}
	
	public double calculateDistanceFromCentre(Point p) {
        return Math.sqrt(Math.pow(center.getX() - p.getX(), 2) + Math.pow(center.getY() - p.getY(), 2));
    }

	public List<Point> getPoints() {
		return points;
	}

	public void setPoints(List<Point> points) {
		this.points = points;
	}

	public Point getCenter() {
		return center;
	}

	public void setCenter(Point center) {
		this.center = center;
	}
	
	public double calculateError(){
		double sum = 0;
		for(Point p : points)
			sum += this.calculateDistanceFromCentre(p);
		return sum/points.size();
	}
}
