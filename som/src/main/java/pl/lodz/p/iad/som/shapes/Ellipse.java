package pl.lodz.p.iad.som.shapes;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Ellipse extends Shape {

	private Point center;
	private double firstRadius;
	private double secondRadius;

	public Ellipse(Point center, double firstRadius, double secondRadius) {
		this.center = center;
		this.firstRadius = firstRadius;
		this.secondRadius = secondRadius;
	}

	@Override
	public Point generatePoint() {
		Random rand = new Random();

		double x, y;
		double angle = rand.nextDouble() * 2 * Math.PI;
		double randFirstRadius = rand.nextDouble() * this.firstRadius;
		double randSecondRadius = rand.nextDouble() * this.secondRadius;

		x = this.center.getX() + randFirstRadius * Math.cos(angle);
		y = this.center.getY() + randSecondRadius * Math.sin(angle);

		System.out.println("[" + x + "," + y + "]");

		return new Point(x, y);
	}

	@Override
	public List<Point> generatePoints(int amount) {
		List<Point> points = new ArrayList<>();

		for (int i = 0; i < amount; i++) {
			points.add(this.generatePoint());
		}

		return points;
	}

}
