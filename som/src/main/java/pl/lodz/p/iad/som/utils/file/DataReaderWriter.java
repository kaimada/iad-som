package pl.lodz.p.iad.som.utils.file;

import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DataReaderWriter {
	
	public List<Point2D.Double> readPointsFromFile(String fileName){
		
		File file = new File(fileName);
		List<Point2D.Double> dataList = new ArrayList<>();
		
		
		try(BufferedReader br = new BufferedReader(new FileReader(file))) {
			
		    for(String line; (line = br.readLine()) != null; ) {
		        String[] values = line.split(" ");
		        dataList.add(new Point2D.Double(Double.parseDouble(values[0]), Double.parseDouble(values[1])));
		    }
		    
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return dataList;
	}
}
