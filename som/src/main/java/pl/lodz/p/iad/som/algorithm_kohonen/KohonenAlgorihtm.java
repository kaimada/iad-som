package pl.lodz.p.iad.som.algorithm_kohonen;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;

import pl.lodz.p.iad.som.algorithm.Algorithm;
import pl.lodz.p.iad.som.configuration.Configuration;
import pl.lodz.p.iad.som.neurons.SimpleNeuralCell;
import pl.lodz.p.iad.som.plot.MyMagicPlot;
import pl.lodz.p.iad.som.shapes.Point;

public class KohonenAlgorihtm extends Algorithm {

	private List<SimpleNeuralCell> neurons;
	private List<Point> points;
	private List<Point> errors;
	
	private double maxRange;
	private double minRange;
	
	private double maxSpeed;
	private double minSpeed;
	
	private double minPotential;
	
	private int ageCount;
	
	private double lambda;
	
	private SimpleNeuralCell winner;
	private List<SimpleNeuralCell> neuronsToChange;
	
	private ProximityFunctionType funcType;
	
	public KohonenAlgorihtm(List<SimpleNeuralCell> neurons, List<Point> points, Configuration conf) {
		this.neurons = neurons;
		this.points = points;
		this.setParams(conf);
		this.errors = new ArrayList<>();
		funcType = ProximityFunctionType.GAUSS;
	}
	
	@Override
	public void learn(int iteration) {
		Random rand = new Random();
		Collections.shuffle(points);
		
		double error = 0;
		
		for(int i = 0; i< points.size(); i++){
			// Ustawianie celu
			Point targetPoint = points.get(i);  //rand.nextInt(points.size())
			this.setTargetPoint(targetPoint);
			
			// Zwyciezca
			lambda = maxRange * Math.pow(minRange / maxRange, iteration / (double) ageCount);
			winner = this.findWinner();
			neuronsToChange = this.getNeuronsToChange();
			this.setNeuronsPotetnial(iteration);			// wyliczamy zmeczenie neuronu
			
			error += winner.getDistance(targetPoint);
			
			// Uczenie
			this.changeWeights(iteration, targetPoint);
			
			
			
		
		}
		errors.add(new Point(iteration,error / points.size()));
		//this.calculateError(winner, iteration);
		
	}

	private void setNeuronsPotetnial(int iteration) {
		for (SimpleNeuralCell neuron : neurons) {
			double p;
			if (neuron.equals(winner)) {
				p = neuron.getPotential() - minPotential;
			}
			else {
				p = neuron.getPotential() + 1 / (double) neurons.size();
			}
			neuron.setPotential(p);
		}
	}
	
	private SimpleNeuralCell findWinner() {
		int firstIndex = 0;
		
		SimpleNeuralCell winner = neurons.get(firstIndex);
		double distance = winner.getDistanceFromTarget();
		
		for (int i = firstIndex + 1; i < neurons.size(); i++) {
			SimpleNeuralCell neuron = neurons.get(i);
			double tmp = neuron.getDistanceFromTarget();
			if (tmp < distance && neuron.getPotential() >= minPotential) {
				winner = neuron;
				distance = tmp;
			}
		}
		
		return winner;
	}
	
	private List<SimpleNeuralCell> getNeuronsToChange() {
		List<SimpleNeuralCell> result = new ArrayList<>();
		
		for (SimpleNeuralCell n : neurons) {
			
			Point neuronAsPoint = n.getAsPoint();
			
			if (winner.getDistance(neuronAsPoint) < lambda) {
				result.add(n);
			}
		}
		
		result.add(winner);
		
		return result;
	}
	
	private void changeWeights(int interation, Point targetPoint) {
		for (int i = 0; i < neuronsToChange.size(); i++) {
			double[] w = neuronsToChange.get(i).getWeights();
			
				switch (funcType) {
				case GAUSS:
					w[0] = w[0] + calcSpeedFactor(interation) * proximityGaussFunc(i, interation) * (targetPoint.x - w[0]);
					w[1] = w[1] + calcSpeedFactor(interation) * proximityGaussFunc(i, interation) * (targetPoint.y - w[1]);
					break;
				case RECTANGULAR:
					w[0] = w[0] + calcSpeedFactor(interation) * proximityRectFunc(i, interation) * (targetPoint.x - w[0]);
					w[1] = w[1] + calcSpeedFactor(interation) * proximityRectFunc(i, interation) * (targetPoint.y - w[1]);
					break;
					
				default:
					break;
				}
		}
	}
	
	private void setTargetPoint(Point targetPoint) {
		for (SimpleNeuralCell neuron : neurons) {
			neuron.setTargetPoint(targetPoint);
		}
	}
	
	private double proximityGaussFunc(int neuronIndex, int iteration) {
		SimpleNeuralCell neuron = neuronsToChange.get(neuronIndex);
		Point neuronAnPoint = neuron.getAsPoint();
		double d = winner.getDistance(neuronAnPoint);
		
		return Math.exp((- Math.pow(d, 2)) / (2 * Math.pow(lambda, 2)) );
	}
	
	private double proximityRectFunc(int neuronIndex, int iteration) {

		SimpleNeuralCell neuron = neuronsToChange.get(neuronIndex);
		Point neuronAnPoint = neuron.getAsPoint();
		double d = winner.getDistance(neuronAnPoint);
		
		return (d > lambda) ? 0 : 1;
	}
	
	private double calcSpeedFactor(int iteration) {
		return maxSpeed * Math.pow(minSpeed / maxSpeed, iteration / (double) ageCount);
	}
	
	public void setParams(Configuration conf) {
		this.maxRange = conf.getMaxRange();
		this.minRange = conf.getMinRange();
		this.maxSpeed = conf.getMaxSpeed();
		this.minSpeed = conf.getMinSpeed();
		this.ageCount = conf.getAgeCount();
		this.funcType = conf.getFuncType();
		this.minPotential = conf.getMinPotential();
	}
	
	public void save(String fileName) {
		//neurony jako punkty
		ArrayList<Point> neuronsAsPoints = new ArrayList<>();
		for (SimpleNeuralCell simpleNeuralCell : neurons) {
			neuronsAsPoints.add(simpleNeuralCell.getAsPoint());
		}
		
		MyMagicPlot plot = new MyMagicPlot();
		XYDataset dataset1 = plot.createDataset(points, "punkty");
		XYDataset dataset2 = plot.createDataset(neuronsAsPoints, "neurony");
		XYLineAndShapeRenderer rendere = new XYLineAndShapeRenderer(false,true);
		XYLineAndShapeRenderer rendere2 = new XYLineAndShapeRenderer(false,true);
		rendere.setShape(new Rectangle(4, 4));
		rendere2.setShape(new Rectangle(2, 2));
		JFreeChart chart = plot.createGraph("Kohonen", dataset2, rendere);
		plot.addDataset(chart, dataset1, rendere2);
		plot.saveGraphToJPG(chart, fileName);
	}
	
	public void saveErrorGraph(String fileName){
		MyMagicPlot plot = new MyMagicPlot();
		XYDataset datasetError = plot.createDataset(errors, "Stopień organizacji sieci");
		JFreeChart chart = plot.createGraph("Stopień organizacji Sieci", datasetError, new XYLineAndShapeRenderer(true, false));
		plot.saveGraphToJPG(chart, fileName);
	}
	
	// Getters and Setters
	
	public List<SimpleNeuralCell> getNeurons() {
		return neurons;
	}

	public void setNeurons(List<SimpleNeuralCell> neurons) {
		this.neurons = neurons;
	}

	public List<Point> getPoints() {
		return points;
	}

	public void setPoints(List<Point> points) {
		this.points = points;
	}

}
