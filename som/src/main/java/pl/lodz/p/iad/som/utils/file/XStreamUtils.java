package pl.lodz.p.iad.som.utils.file;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.XStreamException;

public final class XStreamUtils {
  private static final XStream XSTREAM = new XStream();
  
  private XStreamUtils() {
  }
  
  public static void toXMLFile( Object object, String fileName ) throws XStreamException {
    try {
      XSTREAM.toXML( object, new FileWriter( fileName ) );
    } catch ( IOException ex ) {
      throw new XStreamException( "Can't write to file " + fileName, ex );
    }
  }
  
  public static Object fromXMLFile( String fileName ) throws XStreamException {
    Object obj = null;
    
    try {
      File file = new File( fileName );
      file.createNewFile();
      obj = XSTREAM.fromXML( file );
    } catch ( IOException ex ) {
      throw new XStreamException( "Can't read a file " + fileName, ex );
    }
    
    return obj;
  }
}