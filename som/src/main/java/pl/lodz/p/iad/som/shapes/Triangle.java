package pl.lodz.p.iad.som.shapes;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Triangle extends Shape {

	private Point a;
	private Point b;
	private Point c;

	public Triangle(Point a, Point b, Point c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	@Override
	public Point generatePoint() {
		Random rand = new Random();
		
		double x, y;
		double r1, r2;

		r1 = rand.nextDouble();
		r2 = rand.nextDouble();

		x = (1 - Math.sqrt(r1)) * this.a.getX() + (Math.sqrt(r1) * (1 - r2)) * this.b.getX()
				+ (Math.sqrt(r1) * r2) * this.c.getX();

		y = (1 - Math.sqrt(r1)) * this.a.getY() + (Math.sqrt(r1) * (1 - r2)) * this.b.getY()
				+ (Math.sqrt(r1) * r2) * this.c.getY();

		return new Point(x, y);
	}

	@Override
	public List<Point> generatePoints(int amount) {
		List<Point> points = new ArrayList<>();

		for (int i = 0; i < amount; i++) {
			points.add(this.generatePoint());
		}

		return points;
	}

}
