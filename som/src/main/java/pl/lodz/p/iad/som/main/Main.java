package pl.lodz.p.iad.som.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;

import pl.lodz.p.iad.som.algorithm_k_means.KMeansAlgorithm;
import pl.lodz.p.iad.som.algorithm_k_means.MosaicPart;
import pl.lodz.p.iad.som.algorithm_kohonen.KohonenAlgorihtm;
import pl.lodz.p.iad.som.algorithm_neural_gas.NeuralGasAlgorithm;
import pl.lodz.p.iad.som.configuration.Configuration;
import pl.lodz.p.iad.som.neurons.NeuronsGenerator;
import pl.lodz.p.iad.som.neurons.SimpleNeuralCell;
import pl.lodz.p.iad.som.plot.MyMagicPlot;
import pl.lodz.p.iad.som.shapes.Circle;
import pl.lodz.p.iad.som.shapes.Point;
import pl.lodz.p.iad.som.shapes.Rectangle;
import pl.lodz.p.iad.som.shapes.Shape;
import pl.lodz.p.iad.som.shapes.Triangle;
import pl.lodz.p.iad.som.utils.file.DataOperation;

public class Main {
	
	public static void main(String[] args) {
		
//		kMeans();
		
		//neuralGas();
		
		kohonen();
		
		System.out.println("DONE");
	}
	
	private static void kMeans(){
		Configuration conf = Configuration.getConfiguration();
		
		List<SimpleNeuralCell> neurons =  kohonen();
		
		Shape shape = new Rectangle(new Point(0,0), new Point(10,0), new Point(10,10), new Point(0,10));
		//Shape shape = new Circle(new Point(1,1), 20);
		//Shape shape = new Triangle(new Point(0,0), new Point(10,0), new Point(5,10));
		KMeansAlgorithm kma = new KMeansAlgorithm(shape, conf,neurons);
		
		kma.save("results/graph.jpg");
		
		for(int i = 0; i < conf .getAgeCount(); i++){
			kma.learn();
			kma.save("results/graph"+i+".jpg");
		}
		kma.saveErrorGraph();
		
		
	} 
	
	private static void neuralGas() {
		Configuration conf = Configuration.getConfiguration();
		
		List<SimpleNeuralCell> neurons = NeuronsGenerator.getNeurons(conf.getNeuronCount(), 
				new Circle(new Point(0, 0), 2));
		//List<Point> points = getBasicPoints("data/trainingPoints1.txt");
		List<Point> points = getBasicPoints("data/c_5.txt");
		
		//List<Point> points = new Circle(new Point(1,1), 20).generatePoints(conf.getPointCount());
		/*points.addAll(new Circle(new Point(21,1), 5).generatePoints(conf.getPointCount()/3));
		points.addAll(new Circle(new Point(1,-21), 5).generatePoints(conf.getPointCount()/3));*/
		
		//List<Point> points = new Triangle(new Point(0,0), new Point(10,0), new Point(5,5)).generatePoints(conf.getPointCount());
		
		NeuralGasAlgorithm gas = new NeuralGasAlgorithm(neurons, points, conf);
		
		gas.save("results/graph.jpg");
		for (int i = 0; i < conf.getAgeCount(); i++) {
			gas.learn(i);
			
			if (i%1 == 0) gas.save("results/graph_" + i + ".jpg");
		}
		gas.saveErrorGraph("results/error.jpg");
		saveHistogram(points, neurons);
	}
	
	private static List<SimpleNeuralCell>  kohonen() {
		Configuration conf = Configuration.getConfiguration();
		
		/*List<SimpleNeuralCell> neurons = NeuronsGenerator.getNeurons(conf.getNeuronCount(), 
				new Circle(new Point(1, -1), 10));*/
		List<SimpleNeuralCell> neurons = NeuronsGenerator.getNeurons(conf.getNeuronCount(), 
				new Circle(new Point(0, 0.3), 1.5));
		List<Point> points = getBasicPoints("data/c_5.txt");//new Circle(new Point(0, 0), 5).generatePoints(conf.getPointCount());//getBasicPoints();
		
		//List<Point> points = new Rectangle(new Point(0,0), new Point(10,0), new Point(10,10), new Point(0,10)).generatePoints(conf.getPointCount());
		
		KohonenAlgorihtm k = new KohonenAlgorihtm(neurons, points, conf);
		
		k.save("results/graph.jpg");
		for (int i = 0; i < conf.getAgeCount(); i++) {
			k.learn(i);
			
			if (i%1 == 0) k.save("results/graph_" + i + ".jpg");
		}
		
		k.saveErrorGraph("results/error.jpg");
		saveKMeans(points,neurons);
		saveHistogram(points, neurons);
		
		return neurons;
		
	}
	
	private static List<Point> getBasicPoints(String fileName) {
		DataOperation d = new DataOperation();
		Double[] data = d.readData(fileName, ",");
		return d.convertArrayToPoints(data);
	}
	
	private static void saveHistogram(List<Point> points, List<SimpleNeuralCell> neurons){
		List<Double> distances = new ArrayList<>();
		Map<Double, Double> histo = new HashMap<>();
		
		for (Point point : points) {
			double min = Double.MAX_VALUE;
			SimpleNeuralCell winner = null;
			for (SimpleNeuralCell simpleNeuralCell : neurons) {
				double dist = simpleNeuralCell.getDistance(point);
				if (dist < min) {
					min = dist;
					winner = simpleNeuralCell;
				}
			}
			
			distances.add(min);
		}
		
		for (Double double1 : distances) {
			double x = Math.ceil(double1*50)/50;
			if (histo.containsKey(x)) {
				Double value = histo.get(x);
				histo.remove(x);
				histo.put(x, value + 1);
			} else {
				histo.put(x, new Double(1));
			}
		}
		
		List<Point> pointsHisto = new ArrayList<>();
		
		for (Map.Entry<Double, Double> entry : histo.entrySet())
		{
		    pointsHisto.add(new Point(entry.getKey(), entry.getValue()));
		}
		
		MyMagicPlot plot = new MyMagicPlot();
		XYDataset datasetError = plot.createDataset(pointsHisto, "Stopień organizacji sieci");
		
		JFreeChart chart = plot.createGraph("histo", datasetError, new XYLineAndShapeRenderer(false, true));
		plot.saveGraphToJPG(chart, "results/histo.jpg");
	}
	
	private static void saveKMeans(List<Point> points, List<SimpleNeuralCell> neurons){
		double currentDistance, closestDistance;
        
        List<List<Point>> mosaicPartsPointsList = new ArrayList<>();
        
        for(int i = 0;i<neurons.size();i++)
        	mosaicPartsPointsList.add(new ArrayList<Point>());
        	
        for (int i = 0; i < points.size(); i++) {

            closestDistance = Double.MAX_VALUE;
            int closestMosaicPartIndex = 0;

            for (int j = 0; j < neurons.size(); j++) {
            	currentDistance = neurons.get(j).getDistance(points.get(i));
            	
                if(currentDistance < closestDistance){
                	closestDistance = currentDistance;
                	closestMosaicPartIndex = j;
                }
            }

            mosaicPartsPointsList.get(closestMosaicPartIndex).add(points.get(i));
            
        }
		
		MyMagicPlot plot = new MyMagicPlot();
		List<XYDataset> dataSets = new ArrayList<>();
		
		for(int i = 0; i<mosaicPartsPointsList.size();i++)
			dataSets.add(plot.createDataset(mosaicPartsPointsList.get(i), ""));

		List<Point> centers = new ArrayList<>();
		for(SimpleNeuralCell ns : neurons)
			centers.add(ns.getAsPoint());
		
		XYDataset centersDataSet = plot.createDataset(centers, "Centers");
		
		JFreeChart chart = plot.createGraph("Mozaika Woronoja", centersDataSet, new XYLineAndShapeRenderer(false, true));
		
		for(int i = 0;i < dataSets.size(); i++)
			plot.addDataset(chart, dataSets.get(i), new XYLineAndShapeRenderer(false, true));
		
		plot.saveGraphToJPG(chart, "results/voronoj.jpg");
	}
}
