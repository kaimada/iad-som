package pl.lodz.p.iad.som.neurons;

import java.util.Arrays;

import pl.lodz.p.iad.som.shapes.Point;

public class SimpleNeuralCell {

	private double potential = 1;
	
	private double[] weights;
	private Point targetPoint = null;

	public SimpleNeuralCell(double... weights) {
		this.weights = weights;
	}

	public void learn(double speedFactor, double neighbourFuncVal, double... inputs) {
		for (int i = 0; i < this.weights.length; i++) {
			this.weights[i] = this.weights[i] + speedFactor * neighbourFuncVal * (inputs[i] - this.weights[i]); 
		}
	}

	public double getWeight(int index) {
		return weights[index];
	}

	public double[] getWeights() {
		return this.weights;
	}
	
	public Point getAsPoint() {
		if (weights.length != 2) {
			throw new IllegalStateException("Wrong :>");
		}
		
		return new Point(weights[0], weights[1]);
	}
	
	public double getDistance(Point point) {
		if (weights.length != 2) {
			throw new IllegalStateException("Wrong :>");
		}
		
		return Math.sqrt(Math.pow(point.x - weights[0], 2) + Math.pow(point.y - weights[1], 2));
	}

	public double getDistanceFromTarget() {
		if (weights.length != 2) {
			throw new IllegalStateException("Wrong :>");
		}
		
		return Math.sqrt(Math.pow(targetPoint.x - weights[0], 2) + Math.pow(targetPoint.y - weights[1], 2));
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(potential);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((targetPoint == null) ? 0 : targetPoint.hashCode());
		result = prime * result + Arrays.hashCode(weights);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimpleNeuralCell other = (SimpleNeuralCell) obj;
		if (Double.doubleToLongBits(potential) != Double.doubleToLongBits(other.potential))
			return false;
		if (targetPoint == null) {
			if (other.targetPoint != null)
				return false;
		} else if (!targetPoint.equals(other.targetPoint))
			return false;
		if (!Arrays.equals(weights, other.weights))
			return false;
		return true;
	}

	public Point getTargetPoint() {
		return targetPoint;
	}

	public void setTargetPoint(Point targetPoint) {
		this.targetPoint = targetPoint;
	}
	
	public void setPotential(double potential) {
		this.potential = potential;
	}
	
	public double getPotential() {
		return potential;
	}
}

